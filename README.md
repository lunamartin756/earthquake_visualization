# Visualización de sismos en México
Interfaz gráfica de usuario que muestra los sismos registrados en México en el periodo de 1900 - 2023.
Se presentan dos gráficas principales: número de sismos por año y número de sismos por meses.
Para la gráfica del número de sismos por meses se puede realizar un filtrado adicional por magnitud.

<h3> Demostración </h3>
![Registors_de_sismos_en_México_2023-06-04_17-11-33](/uploads/db4d06b78d04b0f91ec5350d34cee552/Registors_de_sismos_en_México_2023-06-04_17-11-33.mp4)

<h3> Propósito de la aplicación</h3>
Se crea esta aplicación para abarcar un poco el tema de la creación de GUI usando Tkinter mediante programación orientada a objetos.
<h3> Dependencias: </h3>
El archivo requirements.txt contiene las dependencias requeridas para correr el código


<h2>Ejecución del programa</h2>
Ejecutar el archivo main.py para correr la aplicación.

<h2>Directorios del proyecto</h2>

<h3>webScarpping</h3>

Esta carpeta contiene el código utilizado para la extracción de datos que se utilizaron para las gráficas.

<h3>data</h3>

Contiene los datos en formato CSV que consulta la aplicación.

<h3>frames</h3>

Contiene el archivo de la ventana principal (MainWindow.py) y los frames para cambiar entre los diferentes filtros 
de gráficas (AnualFrame.py y MonthlyFrame.py).

<h2>Datos</h2>

Los datos utilizados para la aplicación se obtuvieron mediante webscrapping de:
- SSN, sismos Grandes: Universidad Nacional Autónoma de México, Instituto de Geofísica, Servicio Sismológico Nacional, México. Extraído el 19 de abril de 2023 desde http://www2.ssn.unam.mx:8080/sismos-fuertes

Para mayor información sobre el aviso legal, uso de datos y privacidad consultar: 
- http://www.ssn.unam.mx/aviso-legal/

<br>
<b>Author:</b> Martín López Luna
<br>
<b>LinkedIn:</b> https://www.linkedin.com/in/mart%C3%ADn-l%C3%B3pez-luna-60b947223/

