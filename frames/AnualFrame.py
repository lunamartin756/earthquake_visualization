import tkinter as tk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg 
import pandas as pd
import numpy as np
import os

class Anual(tk.Frame):

#=======================================================================================================================================
#================================================== Constructor / Frame ================================================================
#=======================================================================================================================================
    def __init__(self, root, window_width, window_height, cwd):
        super().__init__(root, bg="#CACACA", width=window_width-40)
        self.grid(row=0, column=1,rowspan=2, columnspan=2, sticky="nesw")
        self.grid_propagate(0) # Para evitar que cambie las dimensiones del frame debido al tamaño de los widgets

        self.cwd = cwd
        self.canvas = None

        # Label para descripción de la gráfica
        texto = "Total de sismos que se presentaron en México entre 1900 - 2023. \nSe grafican el número de sismos que hubieron por año, considerando\nunicamente sismos grandes (sismos mayores a una magnitud de 5.5)." 
        label1 = tk.Label(self,text=texto , font=("Bold", 15), fg="#000000", bg="#CACACA", height=7, justify="center")
        label1.grid(row=0, column=0, rowspan=2,columnspan=2,padx=40, pady=10)

#============================================================================================================================
#================================================== Methods  ================================================================
#============================================================================================================================

    def graficar(self, root):
        data_path = os.path.join(self.cwd, "data","sismos_datos.csv")
        print(data_path)
        df = pd.read_csv(data_path)

        # Limpiando datos
        df.replace("~6.4","6.4", inplace=True)
        df["Magnitud"] = df["Magnitud"].astype(float)

        # Se crea un arreglo con los años que si tienen  temblores mayores a 6.5
        lista_verificacion = []
        list = df[df["Magnitud"]>=6.5].groupby(["Año"])["Año"].count().index.to_numpy()
        for i in range(1900,2024):
            if i in list:
                lista_verificacion.append("True")
            else:
                lista_verificacion.append("False")

        lista_verificacion.count("False")
        fechas_eliminar = [i for i,e in enumerate(lista_verificacion) if e == "False"]
        fechas_con_temblores = np.arange(1900,2024)
        fechas_con_temblores = np.delete(fechas_con_temblores, fechas_eliminar)

        fig = plt.figure(figsize=(13,7))
        #plt.plot(fechas_con_temblores, df[df["Magnitud"]>=6.5].groupby(["Año"])["Año"].count(),"*k")
        plt.bar(fechas_con_temblores, df[df["Magnitud"]>=6.5].groupby(["Año"])["Año"].count())
        plt.xlabel("Año", fontdict={"fontname": "Arial", "fontsize":16, "weight":"bold"})
        plt.ylabel("Número de sismos por año", fontdict={"fontname": "Arial", "fontsize":16, "weight":"bold"})
        plt.title("Sismos en México mayores a 6.5 (1900 - 2023)", fontdict={"fontname": "Arial", "fontsize":18, "weight":"bold"})
        plt.grid(True)
        self.canvas = FigureCanvasTkAgg(fig, root)
        self.canvas.get_tk_widget().grid(row=2,column=0, columnspan=3,rowspan=3, sticky="w", padx=27, pady=25)
        plt.close(fig)

    def destruirCanvas(self):
        try:
            self.canvas.get_tk_widget().destroy()
        except(AttributeError):
            pass



