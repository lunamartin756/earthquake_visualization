import tkinter as tk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg 
import pandas as pd
import numpy as np
import os

class Monthly(tk.Frame):

#=======================================================================================================================================
#================================================== Constructor / Frame ================================================================
#=======================================================================================================================================
    def __init__(self, root, window_width, window_height, cwd):
        super().__init__(root, bg="#CACACA", width=window_width-40)
        self.grid(row=0, column=1,rowspan=2, columnspan=2, sticky="nesw")
        self.grid_propagate(0) # Para evitar que cambie las dimensiones del frame debido al tamaño de los widgets

        self.cwd = cwd
        self.root = root

        # Label para descripción de la gráfica
        texto = "Total de sismos que se presentaron en México entre 1900 - 2023 en cada mes.\n (El set de datos adquirido solo considera sismos mayores a 5.5)" 
        label1 = tk.Label(self,text=texto , font=("Bold", 15), fg="#000000", bg="#CACACA", height=2, justify="center")
        label1.grid(row=0, column=0, columnspan=2,padx=15, pady=10)

        label2 = tk.Label(self,text="Filtro: Incluir sismos mayores a: " , font=("Bold", 15), fg="#000000", bg="#CACACA")
        label2.grid(row=1, column=0,sticky="e")
        self.magnitud = tk.Entry(self, font=("Arial", 15), justify="center", width=5)
        self.magnitud.grid(row=1, column=1, sticky="w")

        boton1 = tk.Button(self, text="Graficar", font=("Bold", 15), bd=10, bg="#F7F7F7", activebackground="#E5E5E5", heigh=1, width=8, cursor="hand2",
            command=self.graficar)
        boton1.grid(row=2, column=0, sticky="e", pady=5)


#============================================================================================================================
#================================================== Métodos  ================================================================
#============================================================================================================================

    def graficar(self):
        #magnitud = float(self.magnitud.get())

        data_path = os.path.join(self.cwd, "data","sismos_datos.csv")
        print(data_path)
        df = pd.read_csv(data_path)

        # Limpiando datos
        df.replace("~6.4","6.4", inplace=True)
        df["Magnitud"] = df["Magnitud"].astype(float)

        # Filtrando por magnitud y agrupando por meses
        meses_dict = {1:"Enero", 2:"Febrero", 3:"Marzo", 4:"Abril", 5:"Mayo", 6:"Junio", 7:"Julio", 8:"Agosto", 9:"Sep", 10:"Octubre", 11:"Nov", 12:"Dic"}
        df2 = df[df["Magnitud"]>=float(self.magnitud.get())]
        df2 = df2.groupby(["Mes"])["Mes"].count()
        # Generando una lista con los meses que si contengas sismos mayores a "magnitud"
        df2_index  = sorted([*set(df2.index)])
        meses_aplicables = [meses_dict[key] for key in df2_index]

        # Graficando resultados
        fig = plt.figure(figsize=(13,7))
        plt.bar(np.arange(len(meses_aplicables)), df2)
        plt.xlabel("Mes", fontdict={"fontname": "Arial", "fontsize":16, "weight":"bold"})
        plt.xticks(np.arange(len(meses_aplicables)),meses_aplicables)
        plt.ylabel("Número de sismos por mes", fontdict={"fontname": "Arial", "fontsize":16, "weight":"bold"})
        plt.title("Sismos mayores a " + str(self.magnitud.get()) + " en México (1900 - 2023)", fontdict={"fontname": "Arial", "fontsize":18, "weight":"bold"})
        plt.grid(True)
        canvas = FigureCanvasTkAgg(fig, self.root)
        canvas.get_tk_widget().grid(row=2,column=0, columnspan=3,rowspan=3, sticky="w", padx=27, pady=25)
        plt.close(fig)
