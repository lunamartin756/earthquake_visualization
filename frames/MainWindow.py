import tkinter as tk
from tkinter import IntVar
from frames.AnualFrame import Anual
from frames.MonthlyFrame import Monthly

class Window():

# ==================================================================================================
# ======================================= Constructor ==============================================
# ==================================================================================================
	def __init__(self, cwd):
		self.cwd = cwd

		# Main Window
		self.bg_color = "#E6E6E6"
		self.window_width = 1360
		self.window_height = 960

		# Creación de la ventana
		self.root = tk.Tk()
		self.root.title("Registors de sismos en México")
		self.root.config(bg=self.bg_color)

		# Window position
		self.ws = self.root.winfo_screenwidth()
		self.wh = self.root.winfo_screenheight()
		self.x = (self.ws/2) - (self.window_width/2)
		self.y = (self.wh/2) - (self.window_height/2) - 30
		self.root.geometry('%dx%d+%d+%d' % (self.window_width, self.window_height, self.x, self.y))
		self.root.resizable(False, False)

		# Frame para filtro
		label1 = tk.Label(self.root, text="Gráficas filtradas por:", font=("Arial", 18), fg="#FFFFFF", bg="#233847", height=2,width=40)
		label1.grid(row=0, column=0)

		frame1 = tk.Frame(self.root, bg="#233847",height=4,width=40)
		frame1.grid(row=1, column=0, sticky="nesw")
		label11 = tk.Label(frame1, text=" ", bg="#233847", height=4,width=20)
		label11.grid(row=0, column=0) 

		self.opcion = IntVar()
		self.radio1 = tk.Radiobutton(frame1, text="Sismos por año", variable=self.opcion,value=1, font=("Arial", 16), command=self.changeFrame)
		self.radio1.grid(row=0, column=1, padx=55)
		self.radio2 = tk.Radiobutton(frame1, text="Sismos por meses", variable=self.opcion, value=2, font=("Arial", 16), command=self.changeFrame)
		self.radio2.grid(row=1, column=1, pady=5)

		# Frame para opciones de filtro
		self.frame2 = tk.Frame(self.root, bg="#CACACA", width=self.window_width-40)
		self.frame2.grid(row=0, column=1,rowspan=2, columnspan=2, sticky="nesw")
		self.frame2.grid_propagate(0)

		# ============================== Instancias de todos los frames ==============================
		self.anual = Anual(self.root, self.window_width, self.window_height, self.cwd)
		self.monthly = Monthly(self.root, self.window_width, self.window_height, self.cwd)

		# Se establece un Frame por deafault
		self.frame2.tkraise()

		self.root.mainloop()

# ==================================================================================================
# ========================================== Methods ===============================================
# ==================================================================================================
	def changeFrame(self):
		if (self.opcion.get() == 1):
			self.anual.tkraise()	
			self.anual.graficar(self.root)
		elif (self.opcion.get() == 2):
			self.monthly.tkraise()
			self.anual.destruirCanvas()